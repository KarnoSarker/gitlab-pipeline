const http = require('http');

// create a server object
const server = http.createServer((req, res) => {
  res.write('Hello World!'); // write a response to the client
  res.end(); // end the response
});

// specify the port number to listen on
const port = 3000;

// start the server and listen on the specified port
server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
